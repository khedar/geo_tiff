#!/usr/bin/env python

# Created by: Yogesh Khedar , y.khedar@tu-bs.de
# Usage: python creategtiff.py [flir|phantom]
# Input directory contains the GTiffs. The script
# creates two directories Translated and Rotated for
# processed images. Rotated contains the final images

from sys import argv
from osgeo import gdal, osr
import exiftool,os,math
import logging.config
import yaml

def setup_logging(default_path='logs/logging.yaml',default_level=logging.INFO,
    env_key='LOG_CFG'):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


setup_logging()
logger = logging.getLogger(__name__)
logger.info("                                                               ")
logger.info("***************************************************************")
logger.info("                                                               ")
logger.info("					New run 									")
logger.info("                                                               ")
logger.info("***************************************************************")
logger.warn("Currently only (.tif,.jpg,.jpeg,.JPG,.JPEG) images are supported.")

if len(argv) != 2:
	logger.warn("Usage: python creategtiff.py [flir|phantom]")
	exit(1)

if argv[1]=="flir":
	## Camera Const variables for FLIR
	PIXEL_DIM_X=640
	PIXEL_DIM_Y=512
	FOV_X=45.4
	FOV_Y=34.9
elif argv[1]=="phantom":
	## Camera Const variables for Phantom
	PIXEL_DIM_X = 5472
	PIXEL_DIM_Y = 3648
	FOV_X=73.7
	FOV_Y=53.1
INPUT_DIR=os.path.join(os.getcwd(),'Input')
TRANS_DIR=os.path.join(os.getcwd(),'Translated')
WARPED_DIR=os.path.join(os.getcwd(),'Rotated')
img_extensions = [".tif", ".jpg", ".jpeg", ".JPG", ".JPEG"]

logger.info("Input image directory is: " + INPUT_DIR)
logger.info("Translated image directory is: " + TRANS_DIR)
logger.info("Output image directory is: " + WARPED_DIR)


dirs = [INPUT_DIR,TRANS_DIR,WARPED_DIR]

for directory in dirs:
	if not os.path.exists(directory):
		os.makedirs(directory)

def getExifData(image_path):
	## Read the required EXIF Tags from the files
	logger.info("Getting ExifData from image: "+image_path)
	with exiftool.ExifTool() as et:
	    
	    LONG_CENTER = et.get_tag("GPSLongitude", image_path)
	    LAT_CENTER  = et.get_tag("GPSLatitude", image_path)
	    ALTITUDE    = et.get_tag("GPSAltitude", image_path) # They have put relative alt in gps alt
	    HEADING     = et.get_tag("GPSImgDirection", image_path)
	    ALTITUDE    = et.get_tag("RelativeAltitude", image_path) # They have put relative alt in gps alt
	    HEADING     = et.get_tag("FlightYawDegree", image_path)


	meterPerLongDegree = math.cos(LAT_CENTER * (-math.pi/180) ) * 111321
	meterPerLatDegree  = 111600
	GROUND_WIDTH_OF_IMAGE  = 2 * float(ALTITUDE) * math.tan(FOV_X*(math.pi/180)/2)
	GROUND_HEIGHT_OF_IMAGE = 2 * float(ALTITUDE) * math.tan(FOV_Y*(math.pi/180)/2)
	x0 = LONG_CENTER
	y0 = LAT_CENTER
	ANGLE = float(HEADING) * (-math.pi/180)
	DX = GROUND_WIDTH_OF_IMAGE / 2
	DY = GROUND_HEIGHT_OF_IMAGE/2
	x1d = (-DX * math.cos(ANGLE) - DY * math.sin(ANGLE)   ) / meterPerLongDegree  + x0
	y1d = (-DX * math.sin(ANGLE) + DY * math.cos(ANGLE)   ) / meterPerLatDegree   + y0
	x2d = (DX  * math.cos(ANGLE) - DY * math.sin(ANGLE)   ) / meterPerLongDegree  + x0
	y2d = (DX  * math.sin(ANGLE) + DY * math.cos(ANGLE)   ) / meterPerLatDegree   + y0
	x3d = (-DX * math.cos(ANGLE) + DY * math.sin(ANGLE)   ) / meterPerLongDegree  + x0
	y3d = (-DX * math.sin(ANGLE) - DY * math.cos(ANGLE)   ) / meterPerLatDegree   + y0
	x4d = (DX  * math.cos(ANGLE) + DY * math.sin(ANGLE)   ) / meterPerLongDegree  + x0
	y4d = (DX  * math.sin(ANGLE) - DY * math.cos(ANGLE)   ) / meterPerLatDegree   + y0
	gcpList = [gdal.GCP(x1d,y1d,0,0),
	           gdal.GCP(x2d,y2d,0,PIXEL_DIM_X,0),
			   gdal.GCP(x3d,y3d,0,0,PIXEL_DIM_Y),
			   gdal.GCP(x4d,y4d,0,PIXEL_DIM_X,PIXEL_DIM_Y)]
	logger.debug("Meters/Long-Degree: " + str(meterPerLongDegree) + "Meters/Lat-Degree: " + str(meterPerLatDegree))
	logger.debug("Altitude: " + str(ALTITUDE)    + "Long: " + str(LONG_CENTER) + "Lat: " + str(LAT_CENTER) )
	logger.debug("PixelWidth: " + str(PIXEL_DIM_X) + "PixelHeight is "+str(PIXEL_DIM_Y))
	logger.debug("GroundWidth: " + str(GROUND_WIDTH_OF_IMAGE)+"GroundHeight is "+str(GROUND_HEIGHT_OF_IMAGE))
	logger.debug("Camera FOV: "+str(FOV_X)  + " x " + str(FOV_Y) )
	logger.debug("Heading: "+str(HEADING)+" Degrees or " + str(ANGLE) +" Radians")

	return gcpList

img_list=os.listdir(INPUT_DIR)
for img in img_list:
	if img.endswith(tuple(img_extensions)):
		src=os.path.join(INPUT_DIR,img)
		dst=os.path.join(TRANS_DIR,'translated_'+img)
		warp_dst=os.path.join(WARPED_DIR,'rot_'+img)

		gcpList = getExifData(src)

		ds=gdal.Open(src)
		ds  = gdal.Translate(dst, ds, outputSRS = 'EPSG:4326', GCPs = gcpList,creationOptions = ['COMPRESS=JPEG'], format = 'GTiff')
		ds1=gdal.Warp(warp_dst,ds, creationOptions = ['COMPRESS=JPEG'], format = 'GTiff', resampleAlg = gdal.GRIORA_NearestNeighbour, tps=True, dstNodata = 0)
		ds1=None
